"""Guardian node to control the ISI differential motion."""
from guardian import GuardState, GuardStateDecorator
import cdsutils



''' comment many lines

* DOWN state for resetting and turning *everything* off. done
* LSC DOWN state - turn off all LSC offloading, but leave CPS diffs on.  done
* HAM2-HAM3 CPS diff state done
* HAM4-HAM5 CPS diff state done
* BSC CPS state DONE
* MC2 offloading state
.....later:
* PRCL offloading
* MICH offloading
* SRCL offloading

examples: 

ezca.get_LIGOFilter('SUS-%s_M0_DARM_DAMP_%s'%(optic,mode)).ramp_gain(0, ramp_time=0, wait=False)

ezca.get_LIGOFilter('LSC-REFLBIAS').switch('FM9', 'FM3', 'ON')  
''' # ends the many line comment
# comment

#############################################
# Matrix definitions
#############################################


#CPS DIFF MATRIX
cpsdiff_mtrx = cdsutils.CDSMatrix(
    'ISI-DIFF_CPS_MTRX',
    cols={'HAM2_X':1, 'HAM2_Y':2, 'HAM3_X':3, 'HAM3_Y':4, 'HAM4_X':5, 'HAM4_Y':6, 'HAM5_X':7, 'HAM5_Y':8, 'HAM6_X':9, 'HAM6_Y':10, 'HAM7_X':11, 'HAM7_Y':12, 'HAM8_Y':13, 'ITMY_X':14, 'ITMY_Y':15, 'BS_X':16, 'BS_Y':17, 'ITMX_X':18, 'ITMX_Y':19, 'ETMX_X':20, 'ETMY_Y':21},
    rows={'HAM2_X':1, 'HAM2_Y':2, 'HAM3_X':3, 'HAM3_Y':4, 'HAM4_X':5, 'HAM4_Y':6, 'HAM5_X':7, 'HAM5_Y':8, 'HAM6_X':9, 'HAM6_Y':10, 'HAM7_X':11, 'HAM7_Y':12, 'HAM8_Y':13, 'ITMY_X':14, 'ITMY_Y':15, 'BS_X':16, 'BS_Y':17, 'ITMX_X':18, 'ITMX_Y':19, 'ETMX_X':20, 'ETMY_Y':21}
)


#############################################
# Decorator
#############################################
'''
# To use, put the following above each main and run state:
#    @check_ISI_guardState()
class check_ISI_guardState(GuardStateDecorator):
    def pre_exec(self):
        HAM_ISI_to_check = ['SEI_HAM2', 'SEI_HAM3', 'SEI_HAM4', 'SEI_HAM5', 'SEI_HAM6', 'SEI_HAM7', 'SEI_HAM8']
        TM_ISI_to_check  = ['SEI_ITMX', 'SEI_ITMY', 'SEI_ETMX', 'SEI_ETMY']
        BS_ISI_to_check  = ['SEI_BS']

        okay_flag = True # True = everything is okay

        for ham in HAM_ISI_to_check:
            if ezca['GRD-%s_STATE_S'%ham] != 'ISOLATED':
                okay_flag = False
        for tm in TM_ISI_to_check:
            if ezca['GRD-%s_STATE_S'%tm] != 'FULLY_ISOLATED':
                okay_flag = False 
        for bs in BS_ISI_to_check:
            if not (ezca['GRD-%s_STATE_S'%tm] == 'FULLY_ISOLATED' or ezca['GRD-%s_STATE_S'%tm] == 'ISOLATED_DAMPED' or ezca['GRD-%s_STATE_S'%tm] == 'TURNING_ON_ST2_ISOLATION_LOOPS' 
            or ezca['GRD-%s_STATE_S'%tm] == 'TURNING_OFF_ST2_ISOLATION_LOOPS'):
                okay_flag = False

        if not okay_flag:
            return 'DOWN'
'''        

def isi_guardstate_okay():
    #HAM_ISI_to_check = ['SEI_HAM2', 'SEI_HAM3', 'SEI_HAM4', 'SEI_HAM5', 'SEI_HAM6']
    HAM_ISI_to_check = ['SEI_HAM2', 'SEI_HAM3', 'SEI_HAM4', 'SEI_HAM5', 'SEI_HAM7', 'SEI_HAM8']
    TM_ISI_to_check  = ['SEI_ITMX', 'SEI_ITMY', 'SEI_ETMX', 'SEI_ETMY']
    #TM_ISI_to_check  = ['SEI_ITMX', 'SEI_ITMY', 'SEI_ETMY']
    BS_ISI_to_check  = ['SEI_BS']

    okay_flag = True # True = everything is okay

    for ham in HAM_ISI_to_check:
        if ezca['GRD-%s_STATE_S'%ham] != 'ISOLATED':
            okay_flag = False
    for tm in TM_ISI_to_check:
        if ezca['GRD-%s_STATE_S'%tm] != 'FULLY_ISOLATED':
            okay_flag = False 
    for bs in BS_ISI_to_check:
        if not (ezca['GRD-%s_STATE_S'%bs] == 'FULLY_ISOLATED' or ezca['GRD-%s_STATE_S'%bs] == 'ISOLATED_DAMPED' or ezca['GRD-%s_STATE_S'%bs] ==  'TURNING_ON_ST2_ISOLATION_LOOPS' or ezca['GRD-%s_STATE_S'%bs] ==  'FULLY_ISOLATED_NO_ST2_BOOST' or ezca['GRD-%s_STATE_S'%bs] == 'TURNING_ON_ST2_BOOST' or ezca['GRD-%s_STATE_S'%bs] == 'TURNING_OFF_ST2_BOOST'
        or ezca['GRD-%s_STATE_S'%bs] == 'TURNING_OFF_ST2_ISOLATION_LOOPS'):
            okay_flag = False

    return okay_flag


def check_ISI_guardState():
    class FaultChecker(GuardStateDecorator):
        def pre_exec(self):
            
            if not isi_guardstate_okay():
                return 'DOWN'

    return FaultChecker
            


#############################################
# States
#############################################

nominal = 'FULL_DIFF_CPS'

class INIT(GuardState):
    """This is a state that should be able to bring us back to
    the current configuration that we are in. If the node were
    to suddenly come alive, would it know where to go?
    """
    def main(self):
        return 'DOWN'


class IDLE(GuardState):
    """This is a state that should be our nominal, while we're just hanging out.
    """
    index = 50

    @check_ISI_guardState()
    def main(self):
        pass

    @check_ISI_guardState()
    def run(self):
        return True


class LSC_DOWN(GuardState):
    """This is a state. Fill me with awesome SEI diff code.

    """
    index = 20
    goto = False
    @check_ISI_guardState()
    def main(self):
                        
        #turning LSC gains OFF
        columns = ['1','2','3','4']
        rows = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16'] # As of 29Oct2019, we don't have ETMs in the LSC mtrx.
        for col in columns:
            for row in rows:
                ezca['ISI-DIFF_LSC_FILT_MTRX_%s_%s_GAIN'%(row, col)] = 0
        # Turn off LSC pathway:
        ezca['ISI-DIFF_TRIG_DISABLE'] = 0 # 0 disables the output of the LSC path trigger (stops signals from going through)		

    @check_ISI_guardState()
    def run(self):
        return True


class DOWN(GuardState):
    """This is a state. Fill me with awesome SEI diff code.

    """
    index = 10
    goto = False
    #@check_ISI_guardState()
    def main(self):
        
        #turning signals to limiters OFF
        ezca['ISI-DIFF_CONTROL_TRAMP'] = 10
        ezca['ISI-DIFF_CONTROL_BIT'] = 0 # limiters screen: master output switch
        
        #turning CPS gains OFF
        chamber = ['HAM2','HAM3','HAM4','HAM5','HAM7','ITMX','ITMY','BS']
        direction = ['X','Y']
        for direc in direction:
            for chamb in chamber:
                ezca['ISI-DIFF_%s_CPS_%s_GAIN'%(chamb, direc)] = 0
        # ETMs only have beamline directions
        ezca['ISI-DIFF_ETMY_CPS_Y_GAIN'] = 0
        ezca['ISI-DIFF_ETMX_CPS_X_GAIN'] = 0
        ezca['ISI-DIFF_HAM8_CPS_Y_GAIN'] = 0


        #turning LSC gains OFF
        columns = ['1','2','3','4']
        rows = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16']
        for col in columns:
            for row in rows:
                ezca['ISI-DIFF_LSC_FILT_MTRX_%s_%s_GAIN'%(row, col)] = 0

        # Turn off LSC pathway:
        ezca['ISI-DIFF_TRIG_DISABLE'] = 0 # 0 disables the output of the LSC path trigger (stops signals from going through)

        self.timer['wait'] = 10
        self.counter = 1

    def run(self):
        if not isi_guardstate_okay():
            notify('Chambers not nominal')
            return False
        if self.timer['wait'] and self.counter == 1:
            return True


class MASTER_ON(GuardState):

    index = 15
    @check_ISI_guardState()
    def main(self):
        self.timer['wait'] = 10
        self.counter = 1

    @check_ISI_guardState()
    def run(self):
        if self.timer['wait'] and self.counter == 1:
            # Enable the master output  of the ISI DIFF system.
            # At this point each individual path should be zero, so okay to turn on master output.
            ezca['ISI-DIFF_CONTROL_BIT'] = 1
            self.timer['wait'] = 10
            self.counter += 1 

        if self.timer['wait'] and self.counter == 2:
            return True 

    
class HAM2_HAM3_CPS(GuardState):
    """This is a state to turn on the CPS diff for HAM2-HAM3

    """
    index = 100
    @check_ISI_guardState()
    def main(self):
        
        # mtrx.put(row, column, value to put)   
        cpsdiff_mtrx.put('HAM3_X',[],0) # Make sure row is clear
        cpsdiff_mtrx.put('HAM3_X','HAM2_X',-1)
        cpsdiff_mtrx.put('HAM3_X','HAM3_X',1)

        # instructions for HAM3_X CPS filter
        ezca.get_LIGOFilter('ISI-DIFF_HAM3_CPS_X').switch('FM2', 'ON')

        ezca.get_LIGOFilter('ISI-HAM3_SUSINF_X').ramp_gain(1, ramp_time=5)
        ezca['ISI-DIFF_HAM3_X_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        
        ezca.get_LIGOFilter('ISI-DIFF_HAM3_CPS_X').ramp_gain(1, ramp_time=5)

    @check_ISI_guardState()
    def run(self):
        # Actually turn on the path:

        return True



class HAM4_HAM5_CPS(GuardState):
    """This is a state to turn on the CPS diff for HAM4-HAM5

    """
    index = 120
    @check_ISI_guardState()
    def main(self):

        # mtrx.put(row, column, value to put)
        cpsdiff_mtrx.put('HAM4_Y',[],0) # Make sure row is clear
        cpsdiff_mtrx.put('HAM4_Y','HAM5_Y',-1)
        cpsdiff_mtrx.put('HAM4_Y','HAM4_Y',1)
        # instructions for HAM5_X CPS filter
        ezca.get_LIGOFilter('ISI-DIFF_HAM4_CPS_Y').switch('FM2', 'ON')

        ezca.get_LIGOFilter('ISI-HAM4_SUSINF_Y').ramp_gain(1, ramp_time=5)
        ezca['ISI-DIFF_HAM4_Y_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        ezca.get_LIGOFilter('ISI-DIFF_HAM4_CPS_Y').ramp_gain(1, ramp_time=5)

    @check_ISI_guardState()
    def run(self):
        # Actually turn on the path:

        return True



class BSC_MICH_CPS(GuardState):
    """This is a state to turn on the CPS diff for BS-ITMX and BS-ITMY

    """
    index = 170
    @check_ISI_guardState()
    def main(self):

        # mtrx.put(row, column, value to put)
        cpsdiff_mtrx.put('ITMY_Y',[],0)
        cpsdiff_mtrx.put('ITMY_Y','BS_Y',-1)
        cpsdiff_mtrx.put('ITMY_Y','ITMY_Y',1)

        # instructions for ITMY_Y CPS filter        
        ezca.get_LIGOFilter('ISI-ITMY_ST1_SUSINF_Y').ramp_gain(1, ramp_time=5)

        ezca.get_LIGOFilter('ISI-DIFF_ITMY_CPS_Y').switch('FM3','ON')
               
        ezca['ISI-DIFF_ITMY_Y_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        
        # mtrx.put(row, column, value to put)
        cpsdiff_mtrx.put('ITMX_X',[],0)
        cpsdiff_mtrx.put('ITMX_X','BS_X',-1)
        cpsdiff_mtrx.put('ITMX_X','ITMX_X',1)

        # instructions for ITMX_X CPS filter        
        ezca.get_LIGOFilter('ISI-ITMX_ST1_SUSINF_X').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_ITMX_CPS_X').switch('FM3','ON')

        ezca['ISI-DIFF_ITMX_X_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        ezca.get_LIGOFilter('ISI-DIFF_ITMY_CPS_Y').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_ITMX_CPS_X').ramp_gain(1, ramp_time=5)

    @check_ISI_guardState()
    def run(self):


        return True


class BSC_ARM_CPS(GuardState):
    """This is a state to turn on the CPS diff for ETMX-ITMX and ETMY-ITMY

    """
    index = 180
    @check_ISI_guardState()
    def main(self):

        # mtrx.put(row, column, value to put)
        cpsdiff_mtrx.put('ETMY_Y',[],0)
        cpsdiff_mtrx.put('ETMY_Y','ITMY_Y',-1)
        cpsdiff_mtrx.put('ETMY_Y','ETMY_Y',1)

        # instructions for ITMY_Y CPS filter        
        ezca.get_LIGOFilter('ISI-ETMY_ST1_SUSINF_Y').ramp_gain(1, ramp_time=5)

        ezca.get_LIGOFilter('ISI-DIFF_ETMY_CPS_Y').switch('FM2','ON')
               
        ezca['ISI-DIFF_ETMY_Y_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        
        # mtrx.put(row, column, value to put)
        cpsdiff_mtrx.put('ETMX_X',[],0)
        cpsdiff_mtrx.put('ETMX_X','ITMX_X',-1)
        cpsdiff_mtrx.put('ETMX_X','ETMX_X',1)

        # instructions for ITMX_X CPS filter        
        ezca.get_LIGOFilter('ISI-ETMX_ST1_SUSINF_X').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_ETMX_CPS_X').switch('FM2','ON')

        ezca['ISI-DIFF_ETMX_X_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        ezca.get_LIGOFilter('ISI-DIFF_ETMY_CPS_Y').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_ETMX_CPS_X').ramp_gain(1, ramp_time=5)

    @check_ISI_guardState()
    def run(self):


        return True
'''
class CORNER_DIFF_CPS(GuardState):
    """This is a state to turn on the CPS for IMC SRCL MICH

    """
    index = 190
    @check_ISI_guardState()
    def main(self):
        #IMC        
        # mtrx.put(row, column, value to put)  IMC 
        cpsdiff_mtrx.put('HAM3_X',[],0) # Make sure row is clear
        cpsdiff_mtrx.put('HAM3_X','HAM2_X',-1)
        cpsdiff_mtrx.put('HAM3_X','HAM3_X',1)

        # instructions for HAM3_X CPS filter
        ezca.get_LIGOFilter('ISI-DIFF_HAM3_CPS_X').switch('FM2', 'ON')

        ezca.get_LIGOFilter('ISI-HAM3_SUSINF_X').ramp_gain(1, ramp_time=5)
        ezca['ISI-DIFF_HAM3_X_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        #SRCL
        # mtrx.put(row, column, value to put)   SRCL
        cpsdiff_mtrx.put('HAM4_Y',[],0) # Make sure row is clear
        cpsdiff_mtrx.put('HAM4_Y','HAM5_Y',-1)
        cpsdiff_mtrx.put('HAM4_Y','HAM4_Y',1)
        # instructions for HAM5_X CPS filter
        ezca.get_LIGOFilter('ISI-DIFF_HAM4_CPS_Y').switch('FM2', 'ON')

        ezca.get_LIGOFilter('ISI-HAM4_SUSINF_Y').ramp_gain(1, ramp_time=5)
        ezca['ISI-DIFF_HAM4_Y_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit

        #MICH
        # mtrx.put(row, column, value to put)   MICH
        cpsdiff_mtrx.put('BS_Y',[],0)
        cpsdiff_mtrx.put('BS_Y','ITMY_Y',-1)
        cpsdiff_mtrx.put('BS_Y','BS_Y',1)

        # instructions for ITMY_Y CPS filter        
        ezca.get_LIGOFilter('ISI-BS_ST1_SUSINF_Y').ramp_gain(1, ramp_time=5)

        ezca.get_LIGOFilter('ISI-DIFF_BS_CPS_Y').switch('FM3','ON')
               
        ezca['ISI-DIFF_BS_Y_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        
        # mtrx.put(row, column, value to put)
        cpsdiff_mtrx.put('BS_X',[],0)
        cpsdiff_mtrx.put('BS_X','ITMX_X',-1)
        cpsdiff_mtrx.put('BS_X','BS_X',1)

        # instructions for ITMX_X CPS filter        
        ezca.get_LIGOFilter('ISI-BS_ST1_SUSINF_X').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_BS_CPS_X').switch('FM3','ON')

        ezca['ISI-DIFF_BS_X_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit

        #ARMS
        # mtrx.put(row, column, value to put) ARMS
        #cpsdiff_mtrx.put('ETMY_Y',[],0)
        #cpsdiff_mtrx.put('ETMY_Y','ITMY_Y',-1)
        #cpsdiff_mtrx.put('ETMY_Y','ETMY_Y',1)

        # instructions for ITMY_Y CPS filter        
        #ezca.get_LIGOFilter('ISI-ETMY_ST1_SUSINF_Y').ramp_gain(1, ramp_time=5)

        #ezca.get_LIGOFilter('ISI-DIFF_ETMY_CPS_Y').switch('FM2','ON')
               
        #ezca['ISI-DIFF_ETMY_Y_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        
        # mtrx.put(row, column, value to put)
        #cpsdiff_mtrx.put('ETMX_X',[],0)
        #cpsdiff_mtrx.put('ETMX_X','ITMX_X',-1)
        #cpsdiff_mtrx.put('ETMX_X','ETMX_X',1)

        # instructions for ITMX_X CPS filter        
        #ezca.get_LIGOFilter('ISI-ETMX_ST1_SUSINF_X').ramp_gain(1, ramp_time=5)
        #ezca.get_LIGOFilter('ISI-DIFF_ETMX_CPS_X').switch('FM2','ON')

        #ezca['ISI-DIFF_ETMX_X_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit

        ezca.get_LIGOFilter('ISI-DIFF_HAM3_CPS_X').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_HAM4_CPS_Y').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_BS_CPS_Y').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_BS_CPS_X').ramp_gain(1, ramp_time=5)
        #ezca.get_LIGOFilter('ISI-DIFF_ETMY_CPS_Y').ramp_gain(1, ramp_time=5)
        #ezca.get_LIGOFilter('ISI-DIFF_ETMX_CPS_X').ramp_gain(1, ramp_time=5)
        
    @check_ISI_guardState()
    def run(self):
        # Actually turn on the path:

        return True
'''


class CORNER_DIFF_CPS(GuardState):
    """This is a state to turn on the CPS for IMC SRCL MICH and ARMS

    """
    index = 190
    @check_ISI_guardState()
    def main(self):
        #IMC        
        # mtrx.put(row, column, value to put)  IMC 
        cpsdiff_mtrx.put('HAM3_X',[],0) # Make sure row is clear
        cpsdiff_mtrx.put('HAM3_X','HAM2_X',-1)
        cpsdiff_mtrx.put('HAM3_X','HAM3_X',1)

        # instructions for HAM3_X CPS filter
        ezca.get_LIGOFilter('ISI-DIFF_HAM3_CPS_X').switch('FM2', 'ON')

        ezca.get_LIGOFilter('ISI-HAM3_SUSINF_X').ramp_gain(1, ramp_time=5)
        ezca['ISI-DIFF_HAM3_X_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        #SRCL
        # mtrx.put(row, column, value to put)   SRCL
        cpsdiff_mtrx.put('HAM4_Y',[],0) # Make sure row is clear
        cpsdiff_mtrx.put('HAM4_Y','HAM5_Y',-1)
        cpsdiff_mtrx.put('HAM4_Y','HAM4_Y',1)
        # instructions for HAM5_X CPS filter
        ezca.get_LIGOFilter('ISI-DIFF_HAM4_CPS_Y').switch('FM2', 'ON')

        ezca.get_LIGOFilter('ISI-HAM4_SUSINF_Y').ramp_gain(1, ramp_time=5)
        ezca['ISI-DIFF_HAM4_Y_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit

        #MICH
        # mtrx.put(row, column, value to put)   MICH
        cpsdiff_mtrx.put('BS_Y',[],0)
        cpsdiff_mtrx.put('BS_Y','ITMY_Y',-1)
        cpsdiff_mtrx.put('BS_Y','BS_Y',1)

        # instructions for ITMY_Y CPS filter        
        ezca.get_LIGOFilter('ISI-BS_ST1_SUSINF_Y').ramp_gain(1, ramp_time=5)

        ezca.get_LIGOFilter('ISI-DIFF_BS_CPS_Y').switch('FM3','ON')
               
        ezca['ISI-DIFF_BS_Y_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        
        # mtrx.put(row, column, value to put)
        cpsdiff_mtrx.put('BS_X',[],0)
        cpsdiff_mtrx.put('BS_X','ITMX_X',-1)
        cpsdiff_mtrx.put('BS_X','BS_X',1)

        # instructions for ITMX_X CPS filter        
        ezca.get_LIGOFilter('ISI-BS_ST1_SUSINF_X').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_BS_CPS_X').switch('FM3','ON')

        ezca['ISI-DIFF_BS_X_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit

        #ARMS
        # mtrx.put(row, column, value to put) ARMS
        cpsdiff_mtrx.put('ETMY_Y',[],0)
        cpsdiff_mtrx.put('ETMY_Y','ITMY_Y',-1)
        cpsdiff_mtrx.put('ETMY_Y','ETMY_Y',1)

        # instructions for ITMY_Y CPS filter        
        ezca.get_LIGOFilter('ISI-ETMY_ST1_SUSINF_Y').ramp_gain(1, ramp_time=5)

        ezca.get_LIGOFilter('ISI-DIFF_ETMY_CPS_Y').switch('FM2','ON')
               
        ezca['ISI-DIFF_ETMY_Y_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        
        # mtrx.put(row, column, value to put)
        cpsdiff_mtrx.put('ETMX_X',[],0)
        cpsdiff_mtrx.put('ETMX_X','ITMX_X',-1)
        cpsdiff_mtrx.put('ETMX_X','ETMX_X',1)

        # instructions for ITMX_X CPS filter        
        ezca.get_LIGOFilter('ISI-ETMX_ST1_SUSINF_X').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_ETMX_CPS_X').switch('FM2','ON')

        ezca['ISI-DIFF_ETMX_X_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        ezca.get_LIGOFilter('ISI-DIFF_HAM3_CPS_X').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_HAM4_CPS_Y').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_BS_CPS_Y').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_BS_CPS_X').ramp_gain(1, ramp_time=5)
        #ezca.get_LIGOFilter('ISI-DIFF_ETMY_CPS_Y').ramp_gain(1, ramp_time=5)
        #ezca.get_LIGOFilter('ISI-DIFF_ETMX_CPS_X').ramp_gain(1, ramp_time=5)
        
    @check_ISI_guardState()
    def run(self):
        # Actually turn on the path:

        return True 
    
class FULL_DIFF_CPS(GuardState):
    """This is a state to turn on the CPS for IMC SRCL MICH and ARMS

    """
    index = 200
    @check_ISI_guardState()
    def main(self):
        #IMC        
        # mtrx.put(row, column, value to put)  IMC 
        cpsdiff_mtrx.put('HAM3_X',[],0) # Make sure row is clear
        cpsdiff_mtrx.put('HAM3_X','HAM2_X',-1)
        cpsdiff_mtrx.put('HAM3_X','HAM3_X',1)

        # instructions for HAM3_X CPS filter
        ezca.get_LIGOFilter('ISI-DIFF_HAM3_CPS_X').switch('FM2', 'ON')

        ezca.get_LIGOFilter('ISI-HAM3_SUSINF_X').ramp_gain(1, ramp_time=5)
        ezca['ISI-DIFF_HAM3_X_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        
        #SRCL
        # mtrx.put(row, column, value to put)   SRCL
        cpsdiff_mtrx.put('HAM4_Y',[],0) # Make sure row is clear
        cpsdiff_mtrx.put('HAM4_Y','HAM5_Y',-1)
        cpsdiff_mtrx.put('HAM4_Y','HAM4_Y',1)
        # instructions for HAM5_X CPS filter
        ezca.get_LIGOFilter('ISI-DIFF_HAM4_CPS_Y').switch('FM2', 'ON')

        ezca.get_LIGOFilter('ISI-HAM4_SUSINF_Y').ramp_gain(1, ramp_time=5)
        ezca['ISI-DIFF_HAM4_Y_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        
        #FCL
        # mtrx.put(row, column, value to put)   SRCL
        cpsdiff_mtrx.put('HAM8_Y',[],0) # Make sure row is clear
        cpsdiff_mtrx.put('HAM8_Y','HAM7_Y',-1)
        cpsdiff_mtrx.put('HAM8_Y','HAM8_Y',1)
        # instructions for HAM5_X CPS filter
        ezca.get_LIGOFilter('ISI-DIFF_HAM8_CPS_Y').switch('FM1', 'ON')
        ezca.get_LIGOFilter('ISI-DIFF_HAM8_CPS_Y').switch('FM4', 'ON')
        ezca.get_LIGOFilter('ISI-DIFF_HAM8_CPS_Y').switch('FM5', 'ON')
        ezca.get_LIGOFilter('ISI-HAM8_SUSINF_Y').ramp_gain(1, ramp_time=5)
        ezca['ISI-DIFF_HAM8_Y_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit

        #MICH
        # mtrx.put(row, column, value to put)   MICH
        cpsdiff_mtrx.put('BS_Y',[],0)
        cpsdiff_mtrx.put('BS_Y','ITMY_Y',-1)
        cpsdiff_mtrx.put('BS_Y','BS_Y',1)

        # instructions for ITMY_Y CPS filter        
        ezca.get_LIGOFilter('ISI-BS_ST1_SUSINF_Y').ramp_gain(1, ramp_time=5)

        ezca.get_LIGOFilter('ISI-DIFF_BS_CPS_Y').switch('FM3','ON')
               
        ezca['ISI-DIFF_BS_Y_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        
        # mtrx.put(row, column, value to put)
        cpsdiff_mtrx.put('BS_X',[],0)
        cpsdiff_mtrx.put('BS_X','ITMX_X',-1)
        cpsdiff_mtrx.put('BS_X','BS_X',1)

        # instructions for ITMX_X CPS filter        
        ezca.get_LIGOFilter('ISI-BS_ST1_SUSINF_X').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_BS_CPS_X').switch('FM3','ON')

        ezca['ISI-DIFF_BS_X_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit

        #ARMS
        # mtrx.put(row, column, value to put) ARMS
        cpsdiff_mtrx.put('ETMY_Y',[],0)
        cpsdiff_mtrx.put('ETMY_Y','ITMY_Y',-1)
        cpsdiff_mtrx.put('ETMY_Y','ETMY_Y',1)

        # instructions for ITMY_Y CPS filter        
        ezca.get_LIGOFilter('ISI-ETMY_ST1_SUSINF_Y').ramp_gain(1, ramp_time=5)

        ezca.get_LIGOFilter('ISI-DIFF_ETMY_CPS_Y').switch('FM2','ON')
        ezca.get_LIGOFilter('ISI-DIFF_ETMY_CPS_Y').switch('FM5','ON') 
        ezca.get_LIGOFilter('ISI-DIFF_ETMY_CPS_Y').switch('FM4','ON') 
        ezca['ISI-DIFF_ETMY_Y_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        
        # mtrx.put(row, column, value to put)
        cpsdiff_mtrx.put('ETMX_X',[],0)
        cpsdiff_mtrx.put('ETMX_X','ITMX_X',-1)
        cpsdiff_mtrx.put('ETMX_X','ETMX_X',1)

        # instructions for ITMX_X CPS filter        
        ezca.get_LIGOFilter('ISI-ETMX_ST1_SUSINF_X').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_ETMX_CPS_X').switch('FM2','ON')
        ezca.get_LIGOFilter('ISI-DIFF_ETMX_CPS_X').switch('FM5','ON') 
        ezca.get_LIGOFilter('ISI-DIFF_ETMX_CPS_X').switch('FM4','ON') 

        ezca['ISI-DIFF_ETMX_X_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        
        ezca.get_LIGOFilter('ISI-DIFF_HAM3_CPS_X').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_HAM4_CPS_Y').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_HAM8_CPS_Y').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_BS_CPS_Y').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_BS_CPS_X').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_ETMY_CPS_Y').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_ETMX_CPS_X').ramp_gain(1, ramp_time=5)
        
    @check_ISI_guardState()
    def run(self):
        # Actually turn on the path:

        return True
        
class BSC2_FULL_DIFF_CPS(GuardState):
    """This is a state to turn on the CPS for IMC SRCL MICH and ARMS

    """
    index = 250
    @check_ISI_guardState()
    def main(self):
        #IMC        
        # mtrx.put(row, column, value to put)  IMC 
        cpsdiff_mtrx.put('HAM3_X',[],0) # Make sure row is clear
        cpsdiff_mtrx.put('HAM3_X','BS_X',-1)
        cpsdiff_mtrx.put('HAM3_X','HAM3_X',1)

        # instructions for HAM3_X CPS filter
        ezca.get_LIGOFilter('ISI-DIFF_HAM3_CPS_X').switch('FM2', 'ON')
        ezca.get_LIGOFilter('ISI-DIFF_HAM3_CPS_X').switch('FM4', 'ON')
        ezca.get_LIGOFilter('ISI-DIFF_HAM3_CPS_X').switch('FM5', 'ON')        
        ezca.get_LIGOFilter('ISI-HAM3_SUSINF_X').ramp_gain(1, ramp_time=5)
        ezca['ISI-DIFF_HAM3_X_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit

        # mtrx.put(row, column, value to put)  IMC 
        cpsdiff_mtrx.put('HAM2_X',[],0) # Make sure row is clear
        cpsdiff_mtrx.put('HAM2_X','BS_X',-1)
        cpsdiff_mtrx.put('HAM2_X','HAM2_X',1)

        # instructions for HAM3_X CPS filter
        ezca.get_LIGOFilter('ISI-DIFF_HAM2_CPS_X').switch('FM2', 'ON')
        ezca.get_LIGOFilter('ISI-DIFF_HAM2_CPS_X').switch('FM4', 'ON')
        ezca.get_LIGOFilter('ISI-DIFF_HAM2_CPS_X').switch('FM5', 'ON')
        ezca.get_LIGOFilter('ISI-HAM2_SUSINF_X').ramp_gain(1, ramp_time=5)
        ezca['ISI-DIFF_HAM2_X_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
                
        #SRCL
        # mtrx.put(row, column, value to put)   SRCL
        cpsdiff_mtrx.put('HAM4_Y',[],0) # Make sure row is clear
        cpsdiff_mtrx.put('HAM4_Y','BS_Y',-1)
        cpsdiff_mtrx.put('HAM4_Y','HAM4_Y',1)
        # instructions for HAM5_X CPS filter
        ezca.get_LIGOFilter('ISI-DIFF_HAM4_CPS_Y').switch('FM2', 'ON')
        ezca.get_LIGOFilter('ISI-DIFF_HAM4_CPS_Y').switch('FM4', 'ON')
        ezca.get_LIGOFilter('ISI-DIFF_HAM4_CPS_Y').switch('FM5', 'ON')
        ezca.get_LIGOFilter('ISI-HAM4_SUSINF_Y').ramp_gain(1, ramp_time=5)
        ezca['ISI-DIFF_HAM4_Y_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        
        cpsdiff_mtrx.put('HAM5_Y',[],0) # Make sure row is clear
        cpsdiff_mtrx.put('HAM5_Y','BS_Y',-1)
        cpsdiff_mtrx.put('HAM5_Y','HAM5_Y',1)
        # instructions for HAM5_X CPS filter
        ezca.get_LIGOFilter('ISI-DIFF_HAM5_CPS_Y').switch('FM2', 'ON')
        ezca.get_LIGOFilter('ISI-DIFF_HAM5_CPS_Y').switch('FM4', 'ON')
        ezca.get_LIGOFilter('ISI-DIFF_HAM5_CPS_Y').switch('FM5', 'ON')
        ezca.get_LIGOFilter('ISI-HAM5_SUSINF_Y').ramp_gain(1, ramp_time=5)
        ezca['ISI-DIFF_HAM5_Y_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit

        cpsdiff_mtrx.put('HAM6_Y',[],0) # Make sure row is clear
        cpsdiff_mtrx.put('HAM6_Y','BS_Y',-1)
        cpsdiff_mtrx.put('HAM6_Y','HAM6_Y',1)
        # instructions for HAM5_X CPS filter
        ezca.get_LIGOFilter('ISI-DIFF_HAM6_CPS_Y').switch('FM2', 'ON')
        ezca.get_LIGOFilter('ISI-DIFF_HAM6_CPS_Y').switch('FM4', 'ON')
        ezca.get_LIGOFilter('ISI-DIFF_HAM6_CPS_Y').switch('FM5', 'ON')
        ezca.get_LIGOFilter('ISI-HAM6_SUSINF_Y').ramp_gain(1, ramp_time=5)
        ezca['ISI-DIFF_HAM6_Y_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        
        #FCL
        # mtrx.put(row, column, value to put)   SRCL
        cpsdiff_mtrx.put('HAM8_Y',[],0) # Make sure row is clear
        cpsdiff_mtrx.put('HAM8_Y','HAM7_Y',-1)
        cpsdiff_mtrx.put('HAM8_Y','HAM8_Y',1)
        # instructions for HAM5_X CPS filter
        ezca.get_LIGOFilter('ISI-DIFF_HAM8_CPS_Y').switch('FM2', 'ON')
        ezca.get_LIGOFilter('ISI-DIFF_HAM8_CPS_Y').switch('FM4', 'ON')
        ezca.get_LIGOFilter('ISI-DIFF_HAM8_CPS_Y').switch('FM5', 'ON')
        ezca.get_LIGOFilter('ISI-HAM8_SUSINF_Y').ramp_gain(1, ramp_time=5)
        ezca['ISI-DIFF_HAM8_Y_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit

        #MICH
        cpsdiff_mtrx.put('BS_Y',[],0)   
        cpsdiff_mtrx.put('BS_X',[],0)               
        # mtrx.put(row, column, value to put)   MICH
        
        cpsdiff_mtrx.put('ITMY_Y',[],0)
        cpsdiff_mtrx.put('ITMY_Y','BS_Y',-1)
        cpsdiff_mtrx.put('ITMY_Y','ITMY_Y',1)

        # instructions for ITMY_Y CPS filter        
        ezca.get_LIGOFilter('ISI-ITMY_ST1_SUSINF_Y').ramp_gain(1, ramp_time=5)

        ezca.get_LIGOFilter('ISI-DIFF_ITMY_CPS_Y').switch('FM3','ON')
        ezca.get_LIGOFilter('ISI-DIFF_ITMY_CPS_Y').switch('FM5','ON') 
        ezca.get_LIGOFilter('ISI-DIFF_ITMY_CPS_Y').switch('FM4','ON')
               
        ezca['ISI-DIFF_ITMY_Y_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        
        # mtrx.put(row, column, value to put)
        cpsdiff_mtrx.put('ITMX_X',[],0)
        cpsdiff_mtrx.put('ITMX_X','BS_X',-1)
        cpsdiff_mtrx.put('ITMX_X','ITMX_X',1)

        # instructions for ITMX_X CPS filter        
        ezca.get_LIGOFilter('ISI-ITMX_ST1_SUSINF_X').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_ITMX_CPS_X').switch('FM3','ON')
        ezca.get_LIGOFilter('ISI-DIFF_ITMX_CPS_X').switch('FM5','ON') 
        ezca.get_LIGOFilter('ISI-DIFF_ITMX_CPS_X').switch('FM4','ON')

        ezca['ISI-DIFF_ITMX_X_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit

        #ARMS
        # mtrx.put(row, column, value to put) ARMS
        cpsdiff_mtrx.put('ETMY_Y',[],0)
        cpsdiff_mtrx.put('ETMY_Y','BS_Y',-1)
        cpsdiff_mtrx.put('ETMY_Y','ETMY_Y',1)

        # instructions for ITMY_Y CPS filter        
        ezca.get_LIGOFilter('ISI-ETMY_ST1_SUSINF_Y').ramp_gain(1, ramp_time=5)

        ezca.get_LIGOFilter('ISI-DIFF_ETMY_CPS_Y').switch('FM2','ON')
        ezca.get_LIGOFilter('ISI-DIFF_ETMY_CPS_Y').switch('FM5','ON') 
        ezca.get_LIGOFilter('ISI-DIFF_ETMY_CPS_Y').switch('FM4','ON') 
        ezca['ISI-DIFF_ETMY_Y_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        
        # mtrx.put(row, column, value to put)
        cpsdiff_mtrx.put('ETMX_X',[],0)
        cpsdiff_mtrx.put('ETMX_X','BS_X',-1)
        cpsdiff_mtrx.put('ETMX_X','ETMX_X',1)

        # instructions for ITMX_X CPS filter        
        ezca.get_LIGOFilter('ISI-ETMX_ST1_SUSINF_X').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_ETMX_CPS_X').switch('FM2','ON')
        ezca.get_LIGOFilter('ISI-DIFF_ETMX_CPS_X').switch('FM5','ON') 
        ezca.get_LIGOFilter('ISI-DIFF_ETMX_CPS_X').switch('FM4','ON') 

        ezca['ISI-DIFF_ETMX_X_LIMIT_SMOOTH_LIMIT'] = 10000 #  limiters screen: smooth limit
        
        ezca.get_LIGOFilter('ISI-DIFF_HAM2_CPS_X').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_HAM3_CPS_X').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_HAM4_CPS_Y').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_HAM5_CPS_Y').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_HAM6_CPS_Y').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_HAM8_CPS_Y').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_ITMY_CPS_Y').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_ITMX_CPS_X').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_ETMY_CPS_Y').ramp_gain(1, ramp_time=5)
        ezca.get_LIGOFilter('ISI-DIFF_ETMX_CPS_X').ramp_gain(1, ramp_time=5)
        
    @check_ISI_guardState()
    def run(self):
        # Actually turn on the path:

        return True
        

class CORNER_TO_FULL(GuardState):
    """This is a state to turn on the CPS for IMC SRCL MICH and ARMS

    """
    index = 220
    request=False
    @check_ISI_guardState()
    def main(self):
        ezca.get_LIGOFilter('ISI-DIFF_ETMY_CPS_Y').ramp_gain(1, ramp_time=30)
        ezca.get_LIGOFilter('ISI-DIFF_ETMX_CPS_X').ramp_gain(1, ramp_time=30)
        
    @check_ISI_guardState()
    def run(self):
        # Actually turn on the path:

        return 'FULL_DIFF_CPS'

class FULL_TO_CORNER(GuardState):
    """This is a state to turn on the CPS for IMC SRCL MICH and ARMS

    """
    index = 210
    request=False
    @check_ISI_guardState()
    def main(self):
        ezca.get_LIGOFilter('ISI-DIFF_ETMY_CPS_Y').ramp_gain(0, ramp_time=30)
        ezca.get_LIGOFilter('ISI-DIFF_ETMX_CPS_X').ramp_gain(0, ramp_time=30)
        
    @check_ISI_guardState()
    def run(self):
        # Actually turn on the path:

        return 'CORNER_DIFF_CPS'
 
 
'''class MC2_LSC(GuardState):
    """This is a state. Fill me with awesome SEI diff code.

    """
    index = 17
    def main(self):
        
        

    def run(self):
        return True
'''


#############################################
# Edges
#############################################

# Tell me how the node should move through the states

edges = [('DOWN',            'MASTER_ON'),
        ('MASTER_ON',            'HAM2_HAM3_CPS'),
        ('HAM2_HAM3_CPS',   'DOWN'),
        ('MASTER_ON',            'HAM4_HAM5_CPS'),
        ('HAM4_HAM5_CPS',   'DOWN'),
        ('MASTER_ON',            'BSC_MICH_CPS'),
        ('BSC_MICH_CPS',    'DOWN'),
        ('MASTER_ON',            'BSC_ARM_CPS'),
        ('BSC_ARM_CPS',    'DOWN'),
        ('MASTER_ON',            'FULL_DIFF_CPS'),
        ('FULL_DIFF_CPS',    'DOWN'),
        ('MASTER_ON',            'BSC2_FULL_DIFF_CPS'),
        ('BSC2_FULL_DIFF_CPS',    'DOWN'),        
        ('MASTER_ON',            'CORNER_DIFF_CPS'),
        ('CORNER_DIFF_CPS',    'DOWN'),
        ('CORNER_DIFF_CPS',    'CORNER_TO_FULL'),
        ('CORNER_TO_FULL',    'FULL_DIFF_CPS'),
        ('FULL_DIFF_CPS',    'FULL_TO_CORNER'),
        ('FULL_TO_CORNER',    'CORNER_DIFF_CPS'),
    ]
'''
edges = [('INIT',           'DOWN'),
        ('DOWN',            'MASTER_ON'),
        ('MASTER_ON',       'IDLE'),
        ('LSC_DOWN',        'IDLE'),
        ('IDLE',            'DOWN'),
        ('IDLE',            'HAM2_HAM3_CPS'),
        ('HAM2_HAM3_CPS',   'IDLE'),
        ('IDLE',            'HAM4_HAM5_CPS'),
        ('HAM4_HAM5_CPS',   'IDLE'),
        ('IDLE',            'BSC_MICH_CPS'),
        ('BSC_MICH_CPS',    'IDLE'),
        ('IDLE',            'BSC_ARM_CPS'),
        ('BSC_ARM_CPS',    'IDLE'),
        ('IDLE',            'FULL_DIFF_CPS'),
        ('FULL_DIFF_CPS',    'IDLE'),
        ('IDLE',            'CORNER_DIFF_CPS'),
        ('CORNER_DIFF_CPS',    'IDLE'),
    ]

'''

'''
edges = [('LSC_DOWN','DOWN'),
         ('DOWN','MASTER_ON'),
         ('MASTER_ON','HAM2_HAM3_CPS'),
         ('HAM2_HAM3_CPS', 'HAM4_HAM5_CPS'),
         ('HAM4_HAM5_CPS','BSC_MICH_CPS'),
         ('BSC_MICH_CPS','HAM4_HAM5_CPS'),
         ('HAM4_HAM5_CPS', 'HAM2_HAM3_CPS'),
         ('HAM2_HAM3_CPS', 'MASTER_ON'),
         ('MASTER_ON','DOWN'),
         ('DOWN','LSC_DOWN')]
'''











